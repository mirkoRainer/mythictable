﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MythicTable.GameSession;

namespace MythicTable
{
    public class LivePlayHub : Hub<ILiveClient>
    {
        public IEntityCollection EntityCollection { get; }

        public LivePlayHub(IEntityCollection entities)
        {
            this.EntityCollection = entities;
        }

        [HubMethodName("submitDelta")]
        public async Task<bool> RebroadcastDelta(SessionDelta delta)
        {
            var success = await this.EntityCollection.ApplyDelta(delta.Entities);

            if (success)
            {
                await this.Clients.All.ConfirmDelta(delta);
            }

            return success;
        }
    }
}
